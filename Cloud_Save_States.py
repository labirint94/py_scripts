import os
from configparser import ConfigParser
from ftplib import FTP
from datetime import datetime
import fnmatch
from getpass import getuser

###List:
# 0 dir_path
# 1 abs_path
# 2 date_m
# 3 console name


# The information needed for FTP connection is read from config file and saved
parser = ConfigParser()
parser.read('config.cnf')
hoastname = parser.get('ftp_section', 'hoastname')
port = int(parser.get('ftp_section', 'port'))
userr = parser.get('ftp_section', 'username')
passw = parser.get('ftp_section', 'password')
home = parser.get('ftp_section', 'home')
homesaves = parser.get('folder_structure', 'saveFolder')


# FTP connection is established 
ftp = FTP()
ftp.connect(hoastname, port)
ftp.login(userr, passw)

# Parameters needed for script
'''
retro_path = path where to start searching for .state files
statePattern = pattern for state files
console = list where the console folders not empty are saved to later create their folders in cloud
retro/cloud_dict = dictionaries where the key is the statesave file name and the value is a list containing useful info
'''
retro_path = "/home/user/RetroPie/roms/"
cloud_path = None
statePattern = "*.state*"
consoles = []
retro_dict = {}
cloud_dict = {}


# Function that verifies if given folder exists, if not it creates it
def createFolder(path, folder):
    new_path = "/" + path + "/" + folder
    if new_path not in ftp.nlst(path):
        ftp.mkd(new_path)


# Function that sets username in path
def setUserInPath(path):
    lst = path.split("/")
    lst[2] = getuser()
    newPath = "/".join(lst)
    return newPath


retro_path = setUserInPath(retro_path)

# The script scans for folders that contain a statesave file and adds the console name to the console list
for root, dirs, files in os.walk(retro_path):
    for file in fnmatch.filter(files, statePattern):
        lst = root.split("/")
        consoles.append(lst[5])
    consoles = list(dict.fromkeys(consoles))

# The script creates the folder structure necessary to save the saves
createFolder(home, homesaves)
cloud_path = home + "/" + homesaves
for c in consoles:
    createFolder(cloud_path, c)

# The script scans retro path and adds in retro_dict a list containing useful info for each statesave found
for root, dirs, files in os.walk(retro_path):
    for filename in fnmatch.filter(files, statePattern):
        retro_lst = []
        retro_lst.append(root)
        retro_lst.append(retro_lst[0] + "/" + filename)
        retro_lst.append(datetime.fromtimestamp(os.path.getmtime(retro_lst[0])).strftime("%d %b %Y %H:%M:%S"))
        lst = root.split("/")
        retro_lst.append(lst[5])
        retro_dict[filename] = retro_lst


# The script scans ftp cloud path and adds in retro_dict a list conaining useful info for each statesave found
for c in consoles:
    console_path = cloud_path + "/" + c
    temp_lst = ftp.nlst(console_path)
    for file in fnmatch.filter(temp_lst, statePattern):
        cloud_lst = []
        lst = file.split("/")
        cloud_lst.append("/" + lst[1] + "/" + lst[2] + "/" + lst[3])
        cloud_lst.append(file)
        ftp.cwd("/" + lst[1] + "/" + lst[2] + "/" + lst[3])
        date_m = ftp.sendcmd('MDTM ' + lst[4])
        cloud_lst.append(datetime.strptime(date_m[4:], "%Y%m%d%H%M%S").strftime("%d %b %Y %H:%M:%S"))
        cloud_lst.append(lst[3])
        cloud_dict[lst[4]] = cloud_lst

# The script uploads the state files
for key, value in retro_dict.items():
    try:
        if key in cloud_dict:
            if value[2] > cloud_dict[key][2]:
                file = open(value[1], 'rb')
                ftp.storbinary('STOR ' + cloud_dict[key][1], file)
    except KeyError:
        file = open(value[1], 'rb')
        newpath = "/" + cloud_path + "/" + value[3] + "/" + key
        ftp.storbinary('STOR ' + newpath, file)
    finally:
        file = open(value[1], 'rb')
        newpath = "/" + cloud_path + "/" + value[3] + "/" + key
        ftp.storbinary('STOR ' + newpath, file)

ftp.quit()
